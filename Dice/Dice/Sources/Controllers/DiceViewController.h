/*
 *	DiceViewController.h
 *	Final
 *	
 *	Created by Justin Tolman on 4/17/14.
 *	Copyright 2014 __MyCompanyName__. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <NinevehGL/NinevehGL.h>

@interface DiceViewController : GLKViewController <NGLViewDelegate>
{
@private
	NGLMesh *_diceBox;
    NSMutableArray *_dice;
	NGLCamera *_camera;
}

@end
