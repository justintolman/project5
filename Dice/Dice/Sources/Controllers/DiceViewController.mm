/*
 *	DiceViewController.m
 *	Final
 *	
 *	Created by Justin Tolman on 4/17/14.
 *	Copyright 2014 __MyCompanyName__. All rights reserved.
 */
#include "btBulletDynamicsCommon.h"
#import "DiceViewController.h"
#import "PMesh.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation DiceViewController{
    
    btBroadphaseInterface*                  _broadphase;
    btDefaultCollisionConfiguration*        _collisionConfiguration;
    btCollisionDispatcher*                  _dispatcher;
    btSequentialImpulseConstraintSolver*    _solver;
    btDiscreteDynamicsWorld*                _world;
}

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) drawView
{
	
	[_camera drawCamera];
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) viewDidLoad
{
	// Must call super to agree with the UIKit rules.
	[super viewDidLoad];
    [self initPhysics];
    // Setup our NGLView.
    NGLView *diceView = [[NGLView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50)];
    
    // Set its delegate to self so the drawview method is used.
    diceView.delegate = self;
    
    // Add the new view on top of our existing view.
    [self.view addSubview:diceView];
    diceView.contentScaleFactor = [[UIScreen mainScreen] scale];
    
	// Setting the loading process parameters. To take advantage of the NGL Binary feature,
	// remove the line "kNGLMeshOriginalYes, kNGLMeshKeyOriginal,". Your mesh will be loaded 950% faster.
	NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
							  kNGLMeshOriginalYes, kNGLMeshKeyOriginal,
							  kNGLMeshCentralizeYes, kNGLMeshKeyCentralize,
							  @"0.1", kNGLMeshKeyNormalize,
							  nil];
    
    _diceBox=[[NGLMesh alloc] initWithFile:@"DiceBox.obj" settings:settings delegate:nil];
    [_diceBox scaleToX:20 toY:20 toZ:20];
	_dice=[[NSMutableArray alloc] init];
    for(int d=0;d<6;d++){
        [_dice addObject:[[NGLMesh alloc] initWithFile:@"Dice.obj" settings:settings delegate:nil]];
        [[_dice objectAtIndex:d] translateToX:((d%2)*0.3)-.15 toY:(0.3*(d%3))-.3 toZ:0];
    }
	
	_camera = [[NGLCamera alloc] initWithMeshes:_diceBox, nil];
	[_camera addMeshesFromArray:_dice];
	[_camera autoAdjustAspectRatio:YES animated:YES];
    [_camera lookAtObject:_diceBox];
	
	// Starts the debug monitor.
	//[[NGLDebug debugMonitor] startWithView:(NGLView *)self.view];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return NO;
}

-(void)initPhysics
{
    _broadphase = new btDbvtBroadphase();
    _collisionConfiguration = new btDefaultCollisionConfiguration();
    _dispatcher = new btCollisionDispatcher(_collisionConfiguration);
    _solver = new btSequentialImpulseConstraintSolver();
    _world = new btDiscreteDynamicsWorld(_dispatcher, _broadphase, _solver, _collisionConfiguration);
    _world->setGravity(btVector3(0, -9.8, 0));
}

- (void)dealloc
{
    delete _world;
    delete _solver;
    delete _collisionConfiguration;
    delete _dispatcher;
    delete _broadphase;
}
@end