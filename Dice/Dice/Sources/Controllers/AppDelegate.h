/*
 *	AppDelegate.h
 *	Dice
 *	
 *	Created by Justin Tolman on 4/18/14.
 *	Copyright 2014 __MyCompanyName__. All rights reserved.
 */

#import <UIKit/UIKit.h>

#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
	UIWindow *_window;
	ViewController *_viewController;
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) ViewController *viewController;

@end

