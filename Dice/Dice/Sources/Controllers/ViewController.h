/*
 *	ViewController.h
 *	Dice
 *	
 *	Created by Justin Tolman on 4/18/14.
 *	Copyright 2014 __MyCompanyName__. All rights reserved.
 */

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <NGLViewDelegate>
{
@private
	NGLMesh *_mesh;
	NGLCamera *_camera;
}

@end
