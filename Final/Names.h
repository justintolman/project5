//
//  Names.h
//  Project5
//
//  Created by Justin Tolman on 5/18/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Names : NSObject
@property NSArray *lastNames;
@property NSArray *maleNames;
@property NSArray *femaleNames;

@end
