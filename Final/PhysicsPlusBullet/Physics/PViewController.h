//
//  PViewController.h
//  Physics
//
//  Created by Erik Buck on 6/4/12.
//

#import <GLKit/GLKit.h>

@interface PViewController : GLKViewController

- (IBAction)takeObjectTypeFrom:(id)sender;

@end
