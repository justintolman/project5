//
//  testView.m
//  Final
//
//  Created by Justin Tolman on 4/24/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "testView.h"

@implementation testView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
