//
//  HatViewController.m
//  Final
//
//  Created by Justin Tolman on 4/23/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "HatViewController.h"
#import "RandomNunberFactory.h"

@interface HatViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *wand;
@property (strong, nonatomic) IBOutlet UIImageView *rabbit;
@property (strong, nonatomic) IBOutlet UILabel *resultLabel;
@property (strong, nonatomic) IBOutlet UIImageView *labelParchment;

@end

@implementation HatViewController{
    NSMutableArray *_hatList;
    RandomNunberFactory *_random;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _random=[RandomNunberFactory sharedRandomNunberFactory];
    [self resetHatList];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)drawFromHat:(id)sender {
    if(_hatList.count==0)[self resetHatList];
    NSUInteger drawIndex;
    //drawIndex=[_random generateNumber:0 randMax:_hatList.count-1];
    _resultLabel.text=[_hatList objectAtIndex:drawIndex];
    [_hatList removeObject:[_hatList objectAtIndex:drawIndex]];
}

- (IBAction)pullRabbit:(id)sender {
    //[UIView animateWithDuration:2 delay:0 options: UIViewAnimationCurveEaseOut animations:^{
        
    //} completion:^(BOOL finished){
    //    NSLog(@"Done!");
    //}];
}

-(void)resetHatList{
    _hatList=nil;
    _hatList=[NSMutableArray  arrayWithObjects:@"Mirianna",@"Andrew",@"Drake",@"Michael",@"Floyd",@"Casper", @"Alberto", nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
