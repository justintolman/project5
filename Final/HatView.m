//
//  HatViewController.m
//  Final
//
//  Created by Justin Tolman on 4/23/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "HatView.h"
#import "RandomNunberFactory.h"
#import <QuartzCore/QuartzCore.h>

@interface HatView ()

@property (strong, nonatomic) IBOutlet UIImageView *wand;
@property (strong, nonatomic) IBOutlet UIImageView *rabbit;
@property (strong, nonatomic) IBOutlet UILabel *resultLabel;
@property (strong, nonatomic) IBOutlet UIImageView *labelParchment;

@end

@implementation HatView{
    NSMutableArray *_hatList;
    RandomNunberFactory *_random;
    CALayer* _wandLayer;
    CALayer* _rabbitLayer;
    CALayer* _parchmentLayer;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.layer.backgroundColor = [UIColor orangeColor].CGColor;
        self.layer.cornerRadius = 20.0;
        self.layer.frame = CGRectInset(self.layer.frame, 20, 20);
        CALayer *main = self.layer;
        main.backgroundColor = [UIColor orangeColor].CGColor;
        main.cornerRadius = 20.0;
        main.frame = CGRectInset(main.frame, 20, 20);
        _random=[RandomNunberFactory sharedRandomNunberFactory];
        [self resetHatList];
        _wandLayer=_wand.layer;
        _rabbitLayer=_rabbit.layer;
        _parchmentLayer=_labelParchment.layer;
        [CATransaction begin];
        [CATransaction setValue: (id) kCFBooleanTrue forKey: kCATransactionDisableActions];
        _parchmentLayer.position = CGPointMake(0, 0);
        [CATransaction commit];
    }
    return self;
}

- (IBAction)drawFromHat:(id)sender {
    if(_hatList.count==0)[self resetHatList];
    NSUInteger drawIndex;
    drawIndex=[_random generateInteger:0 randMax:_hatList.count-1];
    _resultLabel.text=[_hatList objectAtIndex:drawIndex];
    [_hatList removeObject:[_hatList objectAtIndex:drawIndex]];
}

- (IBAction)pullRabbit:(id)sender {
    [UIView animateWithDuration:3 animations:^{
        //[_wand setTransform:CGAffineTransformMakeRotation(M_PI_2)];
    } completion:^(BOOL finished){
        [UIView animateWithDuration:3 animations:^{
            [_wand setTransform:CGAffineTransformMakeRotation(0)];
            _rabbit.center=CGPointMake(_rabbit.layer.position.x,_rabbit.layer.position.x-50);
        } completion:^(BOOL finished){
        }];
    }];
}

-(void)resetHatList{
    _hatList=nil;
    _hatList=[NSMutableArray  arrayWithObjects:@"Mirianna",@"Andrew",@"Drake",@"Michael",@"Floyd",@"Casper", @"Alberto", nil];
}

@end
