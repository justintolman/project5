//
//  SelectorViewController.m
//  Final
//
//  Created by Justin Tolman on 4/19/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "SelectorViewController.h"
#import "HatView.h"
#import "RandomNunberFactory.h"

@interface SelectorViewController (){
    NSMutableArray *_labels;
    NSMutableArray *_spinnerList;
    IBOutlet UIView *_spinnerView;
    RandomNunberFactory *_randomizer;
}
@end

@implementation SelectorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}
- (IBAction)spinSpinnerUp:(id)sender {
    NSInteger steps=[_randomizer generateInteger:[_labels count] randMax:7*[_labels count]];
    [self stepUp:steps];
}

-(void)stepUp:(NSUInteger)steps{
    __block CGAffineTransform previousTransform;
    UILabel *last=[_labels objectAtIndex:[_labels count]-1];
    for (UILabel *current in _labels) {
        if(current==[_labels objectAtIndex:0]){
            previousTransform=last.transform;
        }
        [UIView animateWithDuration:0.1 animations:^{
            CGAffineTransform currentTransform=current.transform;
            current.transform=previousTransform;
            previousTransform=currentTransform;
        } completion:^(BOOL finished) {
            if(current==[_labels objectAtIndex:[_labels count]-1] && steps!=-1)[self stepUp:steps-1];
        }];
    }

}

- (IBAction)spinSpinnerDown:(id)sender {
    NSInteger steps=[_randomizer generateInteger:[_labels count] randMax:7*[_labels count]];
    [self stepDown:steps];
}

-(void)stepDown:(NSUInteger)steps{
    __block CGAffineTransform previousTransform;
    UILabel *last=[_labels objectAtIndex:0];
    for (UILabel *current in [_labels reverseObjectEnumerator]) {
        if(current==[_labels objectAtIndex:[_labels count]-1]){
            previousTransform=last.transform;
        }
        [UIView animateWithDuration:0.1 animations:^{
            CGAffineTransform currentTransform=current.transform;
            current.transform=previousTransform;
            previousTransform=currentTransform;
        } completion:^(BOOL finished) {
            if(current==[_labels objectAtIndex:[_labels count]-1] && steps!=-1)[self stepDown:steps-1];
        }];
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self) {
        // Custom initialization
        _randomizer=[RandomNunberFactory sharedRandomNunberFactory];
        _labels=[[NSMutableArray alloc] init];
        UILabel * currentLabel;
        for(int i=0;i<16;i++){
            currentLabel =  [[UILabel alloc] initWithFrame:CGRectMake(-_spinnerView.layer.frame.size.width*1.4, (_spinnerView.layer.frame.size.height/2), _spinnerView.layer.frame.size.width*.8, _spinnerView.layer.frame.size.width*.2)];
            currentLabel.text =[NSString stringWithFormat: @"Result %d",i];
            currentLabel.textAlignment = NSTextAlignmentRight;
            currentLabel.layer.anchorPoint = CGPointMake(-1,0.5);
            currentLabel.transform = CGAffineTransformMakeRotation((i-7)*M_PI_4/8);
            [_labels addObject:currentLabel];
            [_spinnerView addSubview:currentLabel];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
