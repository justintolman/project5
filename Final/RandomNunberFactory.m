//
//  RandomNunberFactory.m
//  Final
//
//  Created by Justin Tolman on 4/13/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <sqlite3.h>
#import "RandomNunberFactory.h"
#import "Names.h"
#define ARC4RANDOM_MAX      0x100000000
@implementation RandomNunberFactory{
    Names *_names;
    NSArray *_MaleNames;
    NSArray *_FemaleNames;
    NSArray *_LastNames;
}

+ (id)sharedRandomNunberFactory {
    static RandomNunberFactory *sharedRandomNunberFactory = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedRandomNunberFactory = [[self alloc] init];
    });
    return sharedRandomNunberFactory;
}

-(id)init{
    self=[super init];
    _names=[[Names alloc]init];
    _MaleNames=[_names maleNames];
    _FemaleNames=[_names femaleNames];
    _LastNames=[_names lastNames];
    return self;
}

-(NSInteger)generateInteger:(NSInteger) rMin randMax: (NSInteger) rMax {
    return arc4random_uniform((rMax+1)-rMin)+rMin;
}

-(double)generateDouble:(NSInteger) rMin randMax: (NSInteger) rMax {
    return((double)arc4random() / ARC4RANDOM_MAX)
    * (rMax - rMin)
    + rMin;
}
-(BOOL) randBOOL{
    return[self generateInteger:0 randMax:1];
}

-(NSString*) aOrB:(NSString *)A optB:(NSString *)B {
    BOOL n=[self randBOOL];
    if(n)return A;
    return B;
}

-(NSString*) stringFromArray:(NSArray *)Array {
    switch ([Array count]) {
        case 0:
            return @"";
            break;
            
        case 1:
            return Array[0];
            break;
            
        case 2:
            return [self aOrB:[Array objectAtIndex: 0] optB:[Array objectAtIndex: 1]];
            break;
            
        default:
            return [Array objectAtIndex: [self generateInteger:0 randMax:[Array count]-1]];
            break;
    }
}

-(NSString *)randomFirstName{
    NSArray *tempArray=[_FemaleNames arrayByAddingObjectsFromArray:_MaleNames];
    return [self stringFromArray:tempArray];
}

-(NSString *)randomLastName{
    return [self stringFromArray:_LastNames];
}
@end
