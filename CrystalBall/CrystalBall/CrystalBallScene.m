//
//  CrystalBallScene.m
//  CrystalBall
//
//  Created by Justin Tolman on 4/18/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "CrystalBallScene.h"
#import "RandomNunberFactory.h"

@implementation CrystalBallScene{
    NSMutableArray *_crystalBallBackgrounds;
    NSMutableArray *_crystalBallTextures;
    NSArray *_fortunes;
    SKSpriteNode *_bgImage;
    NSUInteger _rotationStep;
    SKEmitterNode *_flame;
    SKSpriteNode *crystalBall;
    SKEmitterNode *_magic;
    SKSpriteNode *_result;
    SKLabelNode *_resultLabel;
    BOOL _acceptUserInput;
    RandomNunberFactory *_randomizer;
    
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        _randomizer=[RandomNunberFactory sharedRandomNunberFactory];
        _fortunes=[NSArray arrayWithObjects:@"It is certain",@"It is decidedly so",@"Without a doubt",@"Yes definitely",@"You may rely on it",@"As I see it, yes",@"Most likely",@"Outlook good",@"Yes",@"Signs point to yes",@"Reply hazy try again",@"Ask again later",@"Better not tell you now",@"Cannot predict now",@"Concentrate and ask again",@"Don't count on it",@"My reply is no",@"My sources say no",@"Outlook not so good",@"Very doubtful", nil];
        _crystalBallBackgrounds=[NSMutableArray array];
        _crystalBallTextures=[NSMutableArray array];
        SKTextureAtlas *textureAnimatedAtlas=[SKTextureAtlas atlasNamed:@"CrystalBall"];
        SKTextureAtlas *backgroundAnimatedAtlas=[SKTextureAtlas atlasNamed:@"CrystalBallBackground"];
        int numImages=(int)textureAnimatedAtlas.textureNames.count;
        for(int i=1; i<=numImages; i++){
            NSString *backgroundName=[NSString stringWithFormat:@"CrystalBallBackground%02d", i];
            SKTexture *temp=[backgroundAnimatedAtlas textureNamed:backgroundName];
            [_crystalBallBackgrounds addObject:temp];
            NSString *textureName=[NSString stringWithFormat:@"CrystalBall%02d", i];
            temp=[textureAnimatedAtlas textureNamed:textureName];
            [_crystalBallTextures addObject:temp];
        }
        _acceptUserInput=YES;
        _rotationStep=0;
        _bgImage = [SKSpriteNode spriteNodeWithTexture: [_crystalBallBackgrounds objectAtIndex:_rotationStep]];
        [self addChild:_bgImage];
        _bgImage.position = CGPointMake(self.size.width/2,(self.size.height+100)/2);
        _bgImage.size=self.size;
        
        crystalBall = [SKSpriteNode spriteNodeWithTexture: [_crystalBallTextures objectAtIndex:_rotationStep]];
        [self addChild:crystalBall];
        crystalBall.position = CGPointMake(self.size.width/2,(self.size.height+100)/2);
        crystalBall.size=CGSizeMake(_bgImage.size.height*0.5, _bgImage.size.height*0.5);
        [self fadeIn];
        _magic = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CrystalBallMagic" ofType:@"sks"]];
        _magic.position = _bgImage.position;
        _magic.alpha=0;
        [self addChild:_magic];
        CFURLRef tmp;
        tmp = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef) @"burn", CFSTR ("wav"), NULL);
        AudioServicesCreateSystemSoundID(tmp, &_burnSound);
        CFRelease(tmp);
        tmp = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef) @"gaze", CFSTR ("wav"), NULL);
        AudioServicesCreateSystemSoundID(tmp, &_gazeSound);
        CFRelease(tmp);
        tmp = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef) @"gong", CFSTR ("wav"), NULL);
        AudioServicesCreateSystemSoundID(tmp, &_gongSound);
        CFRelease(tmp);
        tmp = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef) @"reveal", CFSTR ("wav"), NULL);
        AudioServicesCreateSystemSoundID(tmp, &_revealSound);
        CFRelease(tmp);
    }
    return self;
}

-(void)rotateRight{
    
    _rotationStep++;
    double val;
    switch (_rotationStep) {
            
        case 60:
            _rotationStep=0;
            break;
            
        case 1:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.98, _bgImage.size.height*.92);
            [self addChild:_flame];
            break;
            
        case 13:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.98, _bgImage.size.height*.93);
            [self addChild:_flame];
            break;
            
        case 25:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.98, _bgImage.size.height*.92);
            [self addChild:_flame];
            break;
            
        case 37:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.98, _bgImage.size.height*.95);
            [self addChild:_flame];
            break;
            
        case 49:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.98, _bgImage.size.height*.89);
            [self addChild:_flame];
            break;
            
        default:
            if(_rotationStep%12>5)val=-_bgImage.size.height/200;
            else val=_bgImage.size.height/200;
            _flame.position = CGPointMake( _flame.position.x - _bgImage.size.width/8.35, _flame.position.y+val);
            break;
    }
    _bgImage.texture = [_crystalBallBackgrounds objectAtIndex:_rotationStep];
    crystalBall.texture = [_crystalBallTextures objectAtIndex:_rotationStep];
}

-(void)rotateLeft{
    _rotationStep--;
    double val;
    switch (_rotationStep) {
            
        case -1:
            _rotationStep=59;
            break;
            
        case 57:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.02, _bgImage.size.height*.89);
            [self addChild:_flame];
            break;
            
        case 45:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.02, _bgImage.size.height*.95);
            [self addChild:_flame];
            break;
            
        case 33:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.02, _bgImage.size.height*.92);
            [self addChild:_flame];
            break;
            
        case 21:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.02, _bgImage.size.height*.93);
            [self addChild:_flame];
            break;
            
        case 9:
            _flame = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"CandleFlame" ofType:@"sks"]];
            _flame.position = CGPointMake(_bgImage.size.width*.02, _bgImage.size.height*.92);
            [self addChild:_flame];
            break;
            
        default:
            if(_rotationStep%12<5)val=-_bgImage.size.height/200;
            else val=_bgImage.size.height/200;
            _flame.position = CGPointMake( _flame.position.x + _bgImage.size.width/8.35, _flame.position.y+val);
            break;
    }
    _bgImage.texture = [_crystalBallBackgrounds objectAtIndex:_rotationStep];
    crystalBall.texture = [_crystalBallTextures objectAtIndex:_rotationStep];
}
-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

-(void)rotateBackground{
    [_bgImage runAction:[SKAction repeatAction:
                        [SKAction animateWithTextures:_crystalBallBackgrounds
                                        timePerFrame:0.06f
                                                resize:NO
                                                restore:YES] count:8] withKey:@"RotateBackground"];
}

-(void)fadeIn{
    crystalBall.alpha=0.0;
    [crystalBall runAction:[SKAction customActionWithDuration:2 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        if(crystalBall.alpha<0.75)crystalBall.alpha+=0.025;
    }]];
}

-(void)fadeOut{
    crystalBall.alpha=0.0;
    //[crystalBall runAction:[SKAction customActionWithDuration:2 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
    //    if(crystalBall.alpha>0.0)crystalBall.alpha-=0.025;
    //}]];

}

-(void)gaze{
    if(_acceptUserInput){
        _acceptUserInput=NO;
        AudioServicesPlaySystemSound(_gongSound);
        [_magic runAction:[SKAction fadeInWithDuration:2] completion:^(void){
            _result=[SKSpriteNode spriteNodeWithImageNamed:@"burntParchment"];
            _result.alpha=0;
            _result.position = CGPointMake(self.size.width/2,(self.size.height/2)+50);
            _result.size=CGSizeMake(30, 6.6);
            _result.alpha=0;
            [self addChild:_result];
            [_result runAction:[SKAction fadeInWithDuration:1] completion:^(void){
                //[_result runAction:[SKAction rotateByAngle:-M_PI*2 duration:0.1]];
                [_result runAction:[SKAction scaleBy:10 duration:2]];
                [_result runAction:[SKAction moveTo:CGPointMake(self.size.width/2,(self.size.height)-100) duration:2] completion:^(void){
                    _resultLabel = [SKLabelNode labelNodeWithFontNamed:@"Zapfino"];
                    _resultLabel.text = [_randomizer stringFromArray:_fortunes];
                    _resultLabel.fontColor=[UIColor blackColor];
                    _resultLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
                    _resultLabel.fontSize=12;
                    _resultLabel.position = CGPointMake((self.size.width/2)-(_resultLabel.frame.size.width/2),(self.size.height)-110);
                    [self addChild:_resultLabel];
                    _resultLabel.alpha=0;
                    AudioServicesPlaySystemSound(_revealSound);
                    [_resultLabel runAction:[SKAction fadeInWithDuration:2] completion:^(void){
                        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(clearResult) userInfo:nil repeats:NO];
                        }];
            }];
            }];
            [_magic runAction:[SKAction fadeOutWithDuration:2] completion:^(void){
            }];
            AudioServicesPlaySystemSound(_gazeSound);
        }];
    }
}
-(void) clearResult{
    
    SKEmitterNode *burn = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"burn" ofType:@"sks"]];
    [self addChild:burn];
    [burn runAction:[SKAction customActionWithDuration:3 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        float red=CGColorGetComponents(burn.particleColor.CGColor)[0];
        float green=CGColorGetComponents(burn.particleColor.CGColor)[1];
        float blue=CGColorGetComponents(burn.particleColor.CGColor)[2];
        if(red>0)red-=0.01;
        //if(green>0)green-=0.01
        ;
        if(blue<1)blue+=0.01;
        burn.particleColor=[UIColor colorWithRed:red green:green blue:blue alpha:CGColorGetComponents(burn.particleColor.CGColor)[3]];
    }]];
    [_resultLabel runAction:[SKAction fadeOutWithDuration:3]];
    burn.position = _result.position;
    [burn runAction:[SKAction fadeOutWithDuration:3]];
    [_result runAction:[SKAction fadeOutWithDuration:3] completion:^(void){
        [_resultLabel runAction:[SKAction removeFromParent]];
        [_result runAction:[SKAction removeFromParent]];
        [burn runAction:[SKAction removeFromParent]];
        _acceptUserInput=YES;
    }];
    AudioServicesPlaySystemSound(_burnSound);
}
@end
