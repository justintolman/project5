//
//  AppDelegate.h
//  CrystalBall
//
//  Created by Justin Tolman on 4/18/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
