//
//  CrystalBallViewController.m
//  CrystalBall
//
//  Created by Justin Tolman on 4/18/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "CrystalBallViewController.h"
#import "CrystalBallScene.h"

@implementation CrystalBallViewController{
    CrystalBallScene * _scene;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    
    // Create and configure the scene.
    _scene = [CrystalBallScene sceneWithSize:skView.bounds.size];
    _scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:_scene];
}
- (IBAction)XiButton:(id)sender {
    [_scene gaze];
}

- (void)viewDidAppear:(BOOL)animated{
    [self becomeFirstResponder];
    _scene.view.paused = NO;
    [_scene fadeIn];
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [_scene fadeOut];
    _scene.view.paused = YES;
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event{
    [_scene gaze];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint location=[[touches anyObject] locationInView:self.view];
    CGPoint previous=[[touches anyObject] previousLocationInView:self.view];
    if(location.y>(self.view.frame.size.height-100)/2){
        if (location.x - previous.x > 0) {
            [_scene rotateRight];
        } else if (location.x - previous.x < 0) {
            [_scene rotateLeft];
        }
    }else{
        if (location.x - previous.x > 0) {
            [_scene rotateLeft];
        } else if (location.x - previous.x < 0) {
            [_scene rotateRight];
        }
    }
    
    if(location.x<(self.view.frame.size.width)/2){
        if (location.y - previous.y > 0) {
            [_scene rotateRight];
        } else if (location.y - previous.y < 0) {
            [_scene rotateLeft];
        }
    }else{
        if (location.y - previous.y > 0) {
            [_scene rotateLeft];
        } else if (location.y - previous.y < 0) {
            [_scene rotateRight];
        }
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
