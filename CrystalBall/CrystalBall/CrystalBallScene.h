//
//  CrystalBallScene.h
//  CrystalBall
//

//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface CrystalBallScene : SKScene{
	SystemSoundID _burnSound;
	SystemSoundID _gazeSound;
	SystemSoundID _gongSound;
	SystemSoundID _revealSound;
}
-(void)fadeIn;
-(void)fadeOut;
-(void)rotateLeft;
-(void)rotateRight;
-(void)gaze;

@end
