//
//  CoinViewController.m
//  Final
//
//  Created by Justin Tolman on 4/13/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "CoinViewController.h"
#import "CoinScene.h"

@implementation CoinViewController{
    CoinScene *_scene;
    IBOutlet UITableView *coinSelector;
    UITableView *_coinSelector;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _coinSkins=[NSArray arrayWithObjects: @"Penny", @"Nickel", @"Dime", @"Quarter", @"Fifty", @"Two", nil];
    SKView * skView = (SKView *)self.view;
    
    _scene = [CoinScene sceneWithSize:skView.bounds.size];
    _scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:_scene];
    [_coinSelector setAlpha:0];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event{
    [_scene flipCoin];
}

-(void)viewDidAppear:(BOOL)animated{
    [self becomeFirstResponder];
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [_coinSkins count];
    return [_coinSkins count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *coinTableIdentifier = @"CoinTableItem";
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:coinTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:coinTableIdentifier];
    }
    UIImageView *heads=[[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 128, 128)];
    heads.image=[UIImage imageNamed:[[_coinSkins objectAtIndex:indexPath.row] stringByAppendingString:@"0.png"]];
    [cell addSubview:heads];
    UIImageView *tails=[[UIImageView alloc] initWithFrame:CGRectMake(138, 0, 128, 128)];
    tails.image=[UIImage imageNamed:[[_coinSkins objectAtIndex:indexPath.row] stringByAppendingString:@"8.png"]];
    [cell addSubview:tails];
    cell.backgroundColor=[UIColor blackColor];
    return cell;
}
- (IBAction)showCoinSelector:(id)sender {
    [UIView animateWithDuration:.5 animations:^{
        [_coinSelector setAlpha:1];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_scene selectCoinSkin:[_coinSkins objectAtIndex:indexPath.row]];
    [UIView animateWithDuration:.5 animations:^{
        [_coinSelector setAlpha:0];
    }];
    
}

@end
