//
//  CoinDelegate.h
//  Coin
//
//  Created by Justin Tolman on 4/17/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoinDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
