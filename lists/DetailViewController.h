//
//  DetailViewController.h
//  Project5
//
//  Created by Justin Tolman on 5/18/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
